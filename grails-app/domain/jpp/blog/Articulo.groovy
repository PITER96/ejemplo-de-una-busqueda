package jpp.blog

class Articulo {
    
    String contenido
    String titulo
    String copete
    String volanta
    
    static constraints = {
        
        volanta()
        titulo()
        copete()
        contenido()
        
    }
}
