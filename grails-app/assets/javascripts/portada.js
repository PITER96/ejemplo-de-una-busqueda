/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var fila = document.getElementById("fila");

var request = new XMLHttpRequest();
request.open('GET', 'articulo/index.json');
request.responseType = 'json';
request.send();
request.onload = function() {
  var articulos = request.response;
  poblarPortada(articulos);
}

function poblarPortada(articulos){
    for(i = 0; i < articulos.length; i++){
        card = document.createElement("div");
        cardHeader = document.createElement("div");
        cardBody = document.createElement("div");
        cardTitle = document.createElement("div");
        cardText = document.createElement("div");
        enlace = document.createElement("a");
        
        card.setAttribute("class","card");
        cardHeader.setAttribute("class","card-header");
        cardHeader.innerHTML = articulos[i].volanta;
        
        cardBody.setAttribute("class","card-body");
        cardTitle.setAttribute("class","card-title");
        cardTitle.innerHTML = articulos[i].titulo;
        
        cardText.setAttribute("class","card-text");
        enlace.setAttribute("class","btn btn-primary");
    
        cardBody.appendChild(cardTitle);
        cardBody.appendChild(cardText);
        cardBody.appendChild(enlace);
            
        card.appendChild(cardHeader);
        card.appendChild(cardBody);
               
        fila.appendChild(card);
    }
}